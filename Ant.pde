class Ant extends PVector{
    
  public PVector lookAt;
  private color clr;
  
  Ant(int x, int y) {
    super(x, y);
    this.lookAt = new PVector(0, 1);
    this.clr = #FF0000;
  }

  public final void move(CellState st) {
    lookAt.rotate(st.angle);
    lookAt.x = round(lookAt.x);
    lookAt.y = round(lookAt.y);
    this.add(lookAt);
    
    if ((int)this.x < 0) {
      this.x = nbCellH-1;
    } else if ((int)this.x > nbCellH-1) {
      this.x = 0;
    }
    
    if ((int)this.y < 0) {
      this.y = nbCellV-1;
    } else if ((int)this.y > nbCellV-1) {
      this.y = 0;
    }    
  }  
}
