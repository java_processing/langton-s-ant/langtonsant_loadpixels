enum CellState {   
  
  /*    ///  RRLRLLRLRR
  Black(#292929, -PI/2),
  White(#FFFFFF, -PI/2),
   
  Red(#FF0000, PI/2),
  Green(#00FF00, -PI/2),
  Blue(#0000FF, PI/2),
  
  Yellow(#FFFF00, PI/2),
  Cyan(#00FFFF, -PI/2),
  Magenta(#FF00FF, PI/2),
  
  Orange(#FF7E00, -PI/2),
  Purple(#7E00FF, -PI/2),
  RoyalBlue(#007EFF, -PI/2),    // useless
   
  Grey(#7E7E7E, -PI/2)    // useless
  */
  
  
  /*    ///  LRRRRRLLR /// LR
  Black(#292929, PI/2),
  White(#FFFFFF, -PI/2),
   
  Red(#FF0000, -PI/2),
  Green(#00FF00, -PI/2),
  Blue(#0000FF, -PI/2),
  
  Yellow(#FFFF00, -PI/2),
  Cyan(#00FFFF, PI/2),
  Magenta(#FF00FF, PI/2),
  
  Orange(#FF7E00, -PI/2),
  Purple(#7E00FF, -PI/2),    // useless
  RoyalBlue(#007EFF, -PI/2),    // useless
   
  Grey(#7E7E7E, -PI/2)    // useless
  */
  
  /*  ///  LLRRRLRRRRR
  Black(#292929, PI/2),
  White(#FFFFFF, PI/2),
   
  Red(#FF0000, -PI/2),
  Green(#00FF00, -PI/2),
  Blue(#0000FF, -PI/2),
  
  Yellow(#FFFF00, PI/2),
  Cyan(#00FFFF, -PI/2),
  Magenta(#FF00FF, -PI/2),
  
  Orange(#FF7E00, -PI/2),
  Purple(#7E00FF, -PI/2),
  RoyalBlue(#007EFF, -PI/2),
   
  Grey(#7E7E7E, -PI/2)    // useless
  */
  
      ///  LRRRRLLLRRR  //  LR
  Black(#292929, PI/2),
  White(#FFFFFF, -PI/2),
   
  Red(#FF0000, -PI/2),
  Green(#00FF00, -PI/2),
  Blue(#0000FF, -PI/2),
  
  Yellow(#FFFF00, PI/2),
  Cyan(#00FFFF, PI/2),
  Magenta(#FF00FF, PI/2),
  
  Orange(#FF7E00, -PI/2),
  Purple(#7E00FF, -PI/2),
  RoyalBlue(#007EFF, -PI/2),
   
  Grey(#7E7E7E, -PI/2)    // useless
  
  
  
  /*  ///  RLLLLLLRRLRR
  Black(#292929, -PI/2),
  White(#FFFFFF, PI/2),
   
  Red(#FF0000, PI/2),
  Green(#00FF00, PI/2),
  Blue(#0000FF, PI/2),
  
  Yellow(#FFFF00, PI/2),
  Cyan(#00FFFF, PI/2),
  Magenta(#FF00FF, -PI/2),
  
  Orange(#FF7E00, -PI/2),
  Purple(#7E00FF, PI/2),
  RoyalBlue(#007EFF, -PI/2),
   
  Grey(#7E7E7E, -PI/2)
  */

  /*  ///  LLRRRLRLRLLR  ///  LLRR
  Black(#292929, PI/2), 
  White(#FFFFFF, PI/2), // PI/2

  Red(#FF0000, -PI/2), 
  Green(#00FF00, -PI/2), 
  Blue(#0000FF, -PI/2), 

  Yellow(#FFFF00, PI/2), 
  Cyan(#00FFFF, -PI/2), 
  Magenta(#FF00FF, PI/2), 

  Orange(#FF7E00, -PI/2), 
  Purple(#7E00FF, PI/2), 
  RoyalBlue(#007EFF, PI/2), 

  Grey(#7E7E7E, -PI/2)
  */
  ;

  public color clr;
  public float angle;

  private CellState(color c, float a) {
    this.clr = c;
    this.angle = a;
  }

  public final CellState nextState() {

    switch(this) {
    case Black :
      return White;

    case White :
      return Red;   // Red

    case Red :
      return Green;   // Green

    case Green :
      return Blue;   // Blue

    case Blue :
      return Yellow;   // Yellow

    case Yellow :
      return Cyan;   // Cyan

    case Cyan :
      return Magenta;   // Magenta

    case Magenta :
      return Orange;   // Orange

    case Orange :
      return Purple;   // Purple

    case Purple :
      return RoyalBlue;   // RoyalBlue

    case RoyalBlue :
      return Black;   // Grey

    case Grey :
      return Black;

    default :
      return Black;
    }
  }
}
