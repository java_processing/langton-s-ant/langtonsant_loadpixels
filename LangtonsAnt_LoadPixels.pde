 //<>//
int nbCellH;
int nbCellV;
CellState[][] cellStates;
Ant ant;
PImage displayed;
int speed;
float rectSide;
boolean pause = false;



void setup() {
  frameRate(Float.POSITIVE_INFINITY); //Float.POSITIVE_INFINITY
  fullScreen();
  //size(900, 450);

  nbCellH = 800;
  nbCellV = nbCellH * height / width;
  rectSide = width/((float)nbCellH);

  cellStates = new CellState[nbCellH][nbCellV];
  displayed = createImage(nbCellH, nbCellV, RGB);
  displayed.loadPixels();

  for (int x = 0; x < nbCellH; x++) {
    for (int y = 0; y < nbCellV; y++) {
      int pix = x + y * displayed.width;
      displayed.pixels[pix] = CellState.Black.clr;
      cellStates[x][y] = CellState.Black;
    }
  }
  displayed.updatePixels();
  ant = new Ant(nbCellH/2, nbCellV/2);
}

void draw() {

  if (!pause) {
    background(41);

    displayed.loadPixels();

    speed = 10000;

    for (int iter = 0; iter < speed; iter++) {
      int xant = (int) ant.x;
      int yant = (int) ant.y;
      CellState state = cellStates[xant][yant];

      ant.move(state);
      cellStates[xant][yant] = state.nextState();

      int pix = xant + yant * displayed.width;
      displayed.pixels[pix] = cellStates[xant][yant].clr;
    }
    displayed.updatePixels();
    image(displayed, 0, 0, width, height);
    drawAnt(ant);
  }
}

public void mousePressed() {
  pause = !pause;
}

public void drawAnt(Ant a) {

  float x1, y1, x2, y2, x3, y3;

  x1 = rectSide/4;
  y1 = 0;
  x2 = -rectSide/4;
  y2 = rectSide/4;
  x3 = -rectSide/4;
  y3 = -rectSide/4;

  push();
  translate(rectSide*a.x, rectSide*a.y);
  rotate(a.lookAt.heading());
  fill(a.clr);
  triangle(x1, y1, x2, y2, x3, y3);
  pop();
}
