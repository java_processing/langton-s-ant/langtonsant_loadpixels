# Langton's Ant

Langton's Ant cellular automaton implementation.

Source and documents i used :
* [Langton's Ant](https://www.youtube.com/watch?v=1X-gtr4pEBU)
* [Langton's Ant Wikipedia page](https://en.wikipedia.org/wiki/Langton%27s_ant)
